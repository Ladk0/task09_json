package JSON;

import Gems.Gem;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JsonReader {
    public List<Gem> getGems(File json) {
        ObjectMapper mapper = new ObjectMapper();
        List<Gem> gems = new ArrayList<>();
        try {
            gems = Arrays.asList(mapper.readValue(json, Gem[].class));
        } catch (IOException e) {
            System.out.println(e);
        }
        return gems;
    }
}
