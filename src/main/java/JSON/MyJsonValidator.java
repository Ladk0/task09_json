package JSON;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class MyJsonValidator {
    public boolean validate(File json, File jsonSchema) {
        try {
            JsonNode jsonNode = JsonLoader.fromFile(json);
            JsonNode schemaNode = JsonLoader.fromFile(jsonSchema);

            JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            JsonValidator jsonValidator = factory.getValidator();
            ProcessingReport processingReport = jsonValidator.validate(schemaNode, jsonNode);
            return processingReport.isSuccess();
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
            return false;
        }
    }
}
