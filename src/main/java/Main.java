import Gems.Gem;
import JSON.JsonReader;
import JSON.MyJsonValidator;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        File json = new File("res/Gems.json");
        File jsonSchema = new File("res/GemsSchema.json");

        List<Gem> gems = new JsonReader().getGems(json);

        for (Gem gem : gems)
            System.out.println(gem);

        System.out.println(new MyJsonValidator().validate(json, jsonSchema));
    }
}